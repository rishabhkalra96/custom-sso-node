import express from 'express';
import { saveGrantCodeDetailsToDB, generateGrantCode,  } from './../utility/login-utility/login-utility';
import { IdpHandler } from './../utility/idp/idp_handler';
const loginRouter = express.Router();


loginRouter.route('/')
.get((_req, res) => {
	//render login template
	res.render('login');
})
.post(async (req, res) => {
	if (req.body?.username && req.body?.password) {
		console.log(req.body.username + ' and ' + req.body?.password);
		const idp = new IdpHandler({
			CLIENT_ID: 'ph1smkebi53fhqnfj5cop53p6',
			USER_POOL_ID: 'us-east-1_XM6ENPyvH',
			IDENTITY_POOL_ID: 'us-east-1:cad4968f-036c-4afd-b83d-81503f4423c1',
			type: 'amazon'
		});
		
		const tokenDetails = await idp.validateUser(req.body.username, req.body.password)
		const authDetails = generateGrantCode(tokenDetails);
		
		if(await saveGrantCodeDetailsToDB(authDetails) as any) {
			res.redirect('https://www.example.com?code=' + authDetails['grantCode'], 301)
			return;
		} else {
			res.status(500).json({ok:false, error: 'UNKNOWN_ERROR'})
		}
	}
	res.status(400).json({ok: false, error: 'Username/Password not provided'})
	return;
});

loginRouter.post('/validate-password', async (req,res) => {
	res.status(200).json({ok: true})	
});

loginRouter.get('/token', async (req, res) => {
	// pull code form query params and process
	try {
		const code = req.query.code as any;
		const tokenDetailsToSendBack = await IdpHandler.validateGrantCode(code);
		res.status(200).json({
			ok: true,
			...tokenDetailsToSendBack
		});
	} catch(e) {
		console.log('error in /token api ', e);
		res.status(400).send({
			ok: false, error: e.toString()
		});
	}
});

module.exports = loginRouter;

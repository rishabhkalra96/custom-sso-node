import { mongoConnection } from "../database/mongo";
import {grantCodesModel} from './../models/grant-codes';
import { v4 as uuidv4, validate as uuidValidate } from 'uuid';

export function generateGrantCode(dataForGrantGeneration: any) {
	return {
		grantCode: uuidv4(),
		...dataForGrantGeneration

	};
}

export function isGrantCodeValid(code: string) {
	return uuidValidate(code);
}
export async function saveGrantCodeDetailsToDB(detailsToPersist: any) {
	try {
	await mongoConnection.getInstance();
	const grantCodes = new grantCodesModel(detailsToPersist);
	await grantCodes.save();
	return Promise.resolve(true);
	} catch(e) {
		console.log('error occured while processing db query', e);
		return Promise.reject(false)
	}
}

export async function getTokensUsingGrantCode(code: string) {
	// assuming the code is already validated by uuid
	try {
		await mongoConnection.getInstance();
		const tokenDetails = await grantCodesModel.findOne({
			grantCode: code,
		});
		return tokenDetails;
	} catch(e) {
		console.log('Error occured while retreiving data from database ', e);
		return Promise.reject({ok: false, error: 'UNABLE_TO_RETRIEVE_DETAILS'})
	}
}

export function processTokenDetailsFromDB(dataToProcess) {
	const newData = {};
	if (!dataToProcess) return null;
	if (dataToProcess['username']) {
		newData['username'] = dataToProcess.username;
	}
	if (dataToProcess['jwt']) {
		newData['jwt'] = dataToProcess.jwt;
	}
	if (dataToProcess['refreshToken']) {
		newData['refreshToken'] = dataToProcess.refreshToken;
	}
	if (dataToProcess['tokenExpiry']) {
		newData['tokenExpiry'] = dataToProcess.tokenExpiry;
	}
	newData['code'] = dataToProcess.grantCode
	return newData;
}
const mongoose = require('mongoose');

export class mongoConnection {
	private url: string;
	private user: string;
	private pwd: string;
	private static activeInstance: mongoConnection;
	private mongooseConnection: any;
	
	private constructor() {
		this.url = 'mongodb://localhost:27017/auth-grants';
		this.user= null;
		this.pwd= null;
		this.openConnection();
	}

	static getInstance() {
		if (!mongoConnection.activeInstance) {
			mongoConnection.activeInstance = new mongoConnection();
		}
		return mongoConnection.activeInstance.mongooseConnection;
	}

	private openConnection() {
		return mongoose.connect(this.url)
		.then(_ => {
			console.log('connected to db sucessfully');
			this.mongooseConnection = _;
			return Promise.resolve(true);
		})
		.catch(_connectionErr => {
			console.log('Error occured while trying to connect via mongoose ', _connectionErr);
			this.mongooseConnection = null;
			throw new Error('DB_CONNECTION_FAILURE');
		});
	}

	
}
import express from 'express';
import expressLayouts from 'express-ejs-layouts';
export function setupApplicaiton(app, viewsDirectory, publicAssetDirectory) {
	app.use(express.static(publicAssetDirectory))
	app.use('/css', express.static(publicAssetDirectory + '/css'))
	app.use(expressLayouts);
	app.set('view engine', 'ejs');
	app.set('views', viewsDirectory);
	app.use(require('express').urlencoded({extended: false}));
	app.use(require('express').json());
	// adding basic logger
	app.use((req, res, next) => {
		console.log(`req --> ${req.url}`)
		next();
	})
}
import * as mongoose from "mongoose";

const grantCodesSchema = new mongoose.Schema({
	username: String,
	grantCode: String,
	authTime: Number,
	clientId: String,
	issuer: String,
	jwt: String,
	refreshToken: String,
	scope: String,
	tokenCreatedAt: Number,
	tokenExpiry: Number,
	tokenUse: String,
});
export const grantCodesModel = mongoose.model('grantCodes', grantCodesSchema);
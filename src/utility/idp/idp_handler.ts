import { AuthenticationDetails, CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';
import { getTokensUsingGrantCode, isGrantCodeValid, processTokenDetailsFromDB } from '../login-utility/login-utility';

export class IdpHandler {

	currentIDP: any;
	oAuth: any;
	constructor(idpConfiguration) {
		if (!this.currentIDP) {
			this.currentIDP = this.configureIdps(idpConfiguration);
		}
		return this
		
	}

	private configureIdps(identityCred: any) {
		if (identityCred.type === 'amazon') {
			const PoolData = {
				ClientId: identityCred.CLIENT_ID,
      			UserPoolId: identityCred.USER_POOL_ID,
      			IdentityPoolId: identityCred.IDENTITY_POOL_ID
			  };
			const userPool = new CognitoUserPool(PoolData);
			return {
				type: 'amazon',
				...PoolData,
				cognitoUserPool : userPool,
			};
		}
		return null;
	}

	validateUser(username: string, password: string) {
		return new Promise((resolve, reject) => {
			if (this.currentIDP) {
				if (this.currentIDP.type === 'amazon') {
					const userData = {
						Username: username,
						Pool: this.currentIDP.cognitoUserPool
					  };
					  const authDetails = new AuthenticationDetails({
						  Username: username,
						  Password: password
					  });
					  const cognitoUser = new CognitoUser(userData);
					  cognitoUser.authenticateUser(authDetails, {
						onSuccess: (result) => {
						  const dataToPreserve = {
							  clientID: result.getAccessToken().decodePayload().client_id,
							  username: username,
							  issuer: result.getAccessToken().decodePayload().iss,
							  jwt: result.getAccessToken().getJwtToken(),
							  refreshToken: result.getRefreshToken().getToken(),
							  scope: result.getAccessToken().decodePayload().scope,
							  tokenUse: result.getAccessToken().decodePayload().token_use,
							  authTime: result.getAccessToken().decodePayload().auth_time,
							  tokenCreatedAt: result.getAccessToken().decodePayload().iat,
							  tokenexpiry: result.getAccessToken().decodePayload().exp,
						  }
						  resolve(dataToPreserve);
						},
						onFailure: (err) => {
						  console.error('Error on login auth', userData, err);
						  reject('UNEXPEXTED_ERROR');
						},
					  });
				} else {
					reject('INVALID_IDP')
				}
			}
		});
	}

	static async validateGrantCode(code: string) {
		if (isGrantCodeValid(code)) {
			const tokenDetailsFromDB = await getTokensUsingGrantCode(code);
			const dataTosendBack = processTokenDetailsFromDB(tokenDetailsFromDB);
			if (dataTosendBack) {
				return {
					ok: true,
					...dataTosendBack,
				}
			} else {
				return Promise.reject('NO_TOKEN_AVAILABLE_WITH_THIS_GRANT')
			}
		} else {
			return Promise.reject('INVALID_CODE')
		}
	}
}
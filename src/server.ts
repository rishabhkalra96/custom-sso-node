import express from 'express';
import path from 'path';
const app = express();

import { setupApplicaiton } from './utility/setup';
import { enableRoutes } from './routes/routes';

const port = 3100;
const viewsDirectory = path.join(__dirname, '../src/views');
const publicDirectory = path.join(__dirname, '../src/public');

setupApplicaiton(app, viewsDirectory, publicDirectory);
enableRoutes(app);

app.listen(port, () => {
	console.log(`server on port ${port} is up and running`);
});